#!/sw/lsa/centos7/python-anaconda2/created-20170424/bin/python
# plot results of the scaling tests
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
import numpy as np
import matplotlib.pyplot as plt

from re import findall
numstr = "[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

scatterplot = 1 # versus line plot

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
data = np.empty((1,4))
if 0: 
  fname = 'scaling.ls'
  with open(fname) as f:
    for line in f: 
      if line[:2]=="bh": 
        typ = 1
      elif line[:2]=="n2": 
        typ = 0
      else: 
        print "Unrecognized type: %s" % line[:2]
        raise SystemExit
      # 
      [N,t,dKE] = findall(numstr,line[3:]) # skip the title
      data = np.append(data,[[typ,int(N),float(t),float(dKE)]],axis=0)
else: 
  fname = 'new_scaling.ls'
  with open(fname) as f: 
    for line in f: 
      if line[11:13]=="bh": 
        typ = 1
      elif line[11:13]=="n2": 
        typ = 0
      else: 
        print "Unrecognized type: %s" % line[11:13]
        continue # FIXME raise SystemExit (?)
      # 
      [N,t,dKE] = findall(numstr,line[14:]) # skip the title
      data = np.append(data,[[typ,int(N),float(t),float(dKE)]],axis=0)

data = np.delete(data,0,0) # remove the starter line


# grab data vectors to plot
bhx = data[data[:,0]==1][:,1]
bhy = data[data[:,0]==1][:,2]

n2x = data[data[:,0]==0][:,1]
n2y = data[data[:,0]==0][:,2]

if scatterplot: 
  ax = plt.subplot(111)
  ax.scatter(bhx,bhy,c='b',alpha=.25,label="Barnes--Hut algorithm",
             edgecolor='none')
  ax.scatter(n2x,n2y,c='r',alpha=.25,label="Newtonian algorithm",
             edgecolor='none')
  ax.legend() 
else: 
  bh_plt, = plt.plot(bhx,bhy,c='b',\
                     dashes=[3,1],label="Barnes--Hut algorithm")
  n2_plt, = plt.plot(n2x,n2y,c='r',\
                     dashes=[1,3],label="Newtonian algorithm")
  plt.legend(handles=[bh_plt,n2_plt]) 

plt.title('Step time vs Particle count')
plt.xlabel('N (particle count)')
plt.xscale('log')
plt.ylabel('Step time (seconds)')
plt.yscale('log')
plt.ylim(1e-3,1e+3)

plt.savefig('scaling.png')
plt.show()

raise SystemExit
