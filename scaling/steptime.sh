for i in {2..20..1}; do 
  python barnes_hut.py 0 $i >> new_scaling.ls && python barnes_hut.py 1 >> new_scaling.ls $i; 
done

for i in {20..100..5}; do 
  python barnes_hut.py 0 $i >> new_scaling.ls && python barnes_hut.py 1 >> new_scaling.ls $i; 
done

for i in {100..1000..100}; do 
  python barnes_hut.py 0 $i >> new_scaling.ls && python barnes_hut.py 1 >> new_scaling.ls $i; 
done

for i in {1000..10000..1000}; do 
  python barnes_hut.py 0 $i >> new_scaling.ls && python barnes_hut.py 1 >> new_scaling.ls $i; 
done

for i in {10000..100000..10000}; do 
  python barnes_hut.py 0 $i >> new_scaling.ls && python barnes_hut.py 1 >> new_scaling.ls $i; 
done

for i in {100000..1000000..100000}; do 
  python barnes_hut.py 0 $i >> new_scaling.ls && python barnes_hut.py 1 >> new_scaling.ls $i; 
done

