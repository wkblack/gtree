#!/sw/lsa/centos7/python-anaconda2/created-20170424/bin/python
# plot output from the n-body simulator
perspective=0

from sys import argv
if len(argv)<2 or argv[1]=="bh" or argv[1]=="1":
  treeAlgorithm = 1
  csvtag = "bh"
elif argv[1]=="n2" or argv[1]=="0":
  treeAlgorithm = 0
  csvtag = "n2"
else: 
  print "ERROR: invalid mode"
  raise SystemExit

live = 'live' in argv # live plotting---watch as it goes 

from glob import glob
files=sorted(glob('runs/out_%s_?????.csv' % csvtag))
if len(files)<1: 
  print "ERROR: no files found!"
  raise SystemExit

import pandas as pd
import numpy as np

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from numpy import linspace
from numpy import loadtxt 

# set up plot
if live: plt.ion()
fig = plt.figure()
if perspective:
  ax1 = fig.add_subplot(121,projection='3d') 
  ax2 = fig.add_subplot(122,projection='3d') 
else:
  ax1 = fig.add_subplot(111,projection='3d') 
  # ax1 = Axes3D(fig)
plt.tight_layout() 

if 0: 
  # read off N and L
  from re import findall
  numstr = "[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"
  with open(files[0]) as f: 
    [N,L]=findall(numstr,f.next())
    N=int(N)
    L=float(L) 
else: # hardcode
  N=300 # particle count
  import barnes_hut as bh
  L=bh.LG/50. # boxsize

colors = cm.rainbow(linspace(0, 1, N))

# axis limits
ax1.set_xlim(-L/2,L/2)
ax1.set_ylim(-L/2,L/2)
ax1.set_zlim(-L/2,L/2)
if perspective:
  ax2.set_xlim(-L/2,L/2)
  ax2.set_ylim(-L/2,L/2)
  ax2.set_zlim(-L/2,L/2)
  
def update_plot(q,col): 
  ax1.scatter(q[:,0], q[:,1], q[:,2], c=col)
  if perspective: 
    ax2.scatter(q[:,0], q[:,1], q[:,2], c=col)
    viewangle = 45
    btweyes = 5
    ax1.view_init(30, viewangle-btweyes)
    ax2.view_init(30, viewangle+btweyes)
  if live: 
    plt.draw()
    plt.pause(0.001)

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 

skiprate=5 # up to ten

for ii,f in enumerate(files):
  if 0: # old numpy method
    data=loadtxt(f,skiprows=1,delimiter=',') 
    pos=data[:,0:3]
  else: # new pandas method
    df = pd.read_csv(f)
    if 'x' in df.keys():
      pos = np.transpose([df.x,df.y,df.z])
    else: 
      print "ERROR! Key not found! Use old numpy method?"
      continue
  if live: 
    plt.suptitle(ii)
  if not live or ii % skiprate == 0:
    update_plot(pos,colors)

plt.savefig('out_%s_sum.png' % csvtag)
plt.show() 
