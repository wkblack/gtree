#!/bin/python

N = -1  # let it be set in the code
if __name__=='__main__':
  from sys import argv
  if len(argv)<2 or argv[1]=="bh" or argv[1]=="1":
    treeAlgorithm = 1
    csvtag = "bh"
    print "Running Barnes-Hut"
  elif argv[1]=="n2" or argv[1]=="0":
    treeAlgorithm = 0
    csvtag = "n2"
    print "Running N^2"
  else: 
    print "ERROR: invalid mode"
    raise SystemExit
  
  if len(argv)>2: # read in the next value as number of particles
    N = int(argv[2])
else:  
  print "Assuming Barnes--Hut"
  treeAlgorithm = 1
  csvtag = "bh"

output_dir = 'runs/'
output_fname = output_dir + 'out_%s_%%05i.csv' % csvtag

########################################################################
# Barnes--Hut algorithm https://www.nature.com/articles/324446a0.pdf
# author: William Kevin Black
# modified: 06 Apr 2019
########################################################################
import numpy as np
import pandas as pd
from numpy.linalg import norm
from numpy import pi
from numpy import sqrt
from numpy import savetxt
from sys import stdout # to make pretty print statements
# import numba
########################################################################



########################################################################
# parameters 
########################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# universal parameters 
c = 299792458                           # m/s exactly 
pc = 648000/pi                          # au 
au = 149597870700                       # m
m_in_km = 1000                          # exact
m_in_pc = pc*au                         # exact
m_in_Mpc = m_in_pc*10**6                # exact
km_in_pc = m_in_Mpc*10**-9              # exact
pc_in_Mpc = 1e6                         # exact
s_in_Myr = 60*60*24*365.2422*10**6      # apx
G_SI = 6.67408e-11                      # m^3/kg/s^2 # apx
G_SI_err = 3.1e-15                      # m^3/kg/s^2
M_solar = 1.98847e30                    # kg # apx
M_solar_err = 7e25                      # kg

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# distances (in parsecs): 
AU = 5e-6         # Astronomical Unit (23 AU is ~Centauri orbit)
SS = 1            # Solar System
MW = 32*1e3       # Milky Way
LG = 3.1*1e6      # Local Group
LK = 159*1e6      # Laniakea

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# times (in seconds) 
hr = 60*60
day = hr*24
fortnight = day*14
yr = day*365.2422
decade = 10*yr
century = 100*yr
kyr = yr*1e3
Myr = yr*1e6

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# derived parameters

G_astro = G_SI/m_in_Mpc*M_solar/m_in_km**2*pc_in_Mpc # pc.(km/s)^2/M_sol
G_astro_err = G_astro*sqrt((G_SI_err/G_SI)**2+(M_solar_err/M_solar)**2)
# G_astro ~ .004

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# simulation parameters 

D=3                # dimensionality 
theta_tolerance=.7 # corresponds to about 45 degrees
G=G_astro          # gravitational constant


########################################################################
# Tree code begins here
########################################################################

class Tree: 
  def __init__(self,x,m,xcb,L):
    self.children=[] #np.array([])
    self.N = np.shape(x)[0]
    self.L = L
    
    self.m=np.sum(m)
    self.xcm=np.sum(x*m,axis=0)/self.m
    
    if self.N==1: 
      # assign values 
      self.xcm = x[0]
      self.m = m[0]
    #
    elif self.N > 1: 
      # we need to sub-divide the box
      # for each octant: 
      truthlist = x > xcb # this generates all comparisons necessary below
      for i in range(2): # -x and +x
        for j in range(2): # -y and +y
          for k in range(2): # -z and +z
            # add all x that are in the octant
            within = np.all(truthlist == np.bool_([i,j,k]),axis=1)
            if sum(within)>=1: 
              self.children.append(Tree(x[within],m[within],\
                                     xcb+(np.array([i,j,k])-.5)*L/2,L/2))
            # else the node is empty, and we don't need to make any new things. 
    else: # N<1
      print "this should never happen---we created an empty node" 
      # we're done here. 

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# accelerate particles using BH method
def accelerate(x,tree,epsilon=0): # where epsilon is a softener
  accel=np.zeros(D)
  dx = norm(x-tree.xcm) + epsilon
  if dx>2*epsilon: # then it's likely that the force is not from itself. 
    theta_local = tree.L/dx
    if theta_local<theta_tolerance or tree.N==1: # use the node itself
      accel += G*tree.m*(tree.xcm-x)/dx**3/km_in_pc**2
    else: # we must break up the node
      for child in tree.children:
        accel += accelerate(x,child,epsilon)
  return accel

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# accelerate particles using N^2 Newtonian method
def nsq_accelerate(x,xlist,mlist,epsilon=0): 
  # n^2 acceleration
  accel = np.zeros(D)
  for j in range(len(mlist)): 
    dx = norm(xlist[j]-x) + epsilon
    if dx<2*epsilon: # it's likely the same particle
      continue
    else: 
      accel += mlist[j]*(xlist[j]-x)/dx**3
  return G*accel/km_in_pc**2 # in pc/s^2

########################################################################
# Main sequence of this code
########################################################################

if __name__=='__main__': 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
  # define IC
  
  M = 1                                   # single solar mass
  output_frequency = 100                  # how often to save csvs
  if 0: # use three-body system
    print "Running 3-body system"
    N = 3                                 # particle count
    L = 10*4*23*AU                           # box size
    V = 5/km_in_pc                        # v0 1-sigma in pc/s
    x=(np.random.random((N,D))-.5)*.125*L # initial positions
    dt=fortnight
  elif 0: 
    print "Running default"
    if N==-1: N=10                        # default number of particles
    L = 1e3*SS                            # box size
    V = 100/km_in_pc                      # v0 1-sigma in pc/s
    x=(np.random.random((N,D))-.5)*.25*L  # x0; random in center of box
    dt=2*kyr
    output_frequency = 1
  elif 1: # spherically symmetric problem
    print "Running spherically symmetric collapse"
    N = 300                               # particle count
    L = LG/50.                            # local group size
    V = 1e-30/km_in_pc                    # start from rest
    D = 3                                 # force three dimensions
    costh = 2*np.random.random(N)-1       # random cos(theta) initial positions
    phi0 = 2*np.pi*np.random.random(N)    # random phi initial positions
    r0 = (L/4.0)*np.random.random(N)      # random r initial positions
    x0 = r0*np.sin(np.arccos(costh))*np.cos(phi0)
    y0 = r0*np.sin(np.arccos(costh))*np.sin(phi0)
    z0 = r0*costh
    x = np.transpose([x0,y0,z0])
    dt=4*kyr
    output_frequency=5 #00
    M = 1.5e12 # apx MW mass
    # M = 1.5e13 # 10 times apx MW mass
  else: 
    print "ERROR: unknown starting conditions!"
    raise SystemExit
  
  v=np.random.normal(0,V,(N,D))        # normally distributed
  a=np.zeros((N,D))                    # placeholder 
  m=M*np.ones((N,1))                     # start all as single solar mass
  
  epsilon=L*1e-9                       # lowest probing distance
  epsilon=L*1e-3 # TEMP
  xcb = np.zeros(D)                    # center the box at zero
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # more timing
  
  MAX_STEPS = 5000                     # number of time steps
  
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # main body: 
  
  from time import time
  # run through time steps
  KE_initial = .5*np.sum((m*M_solar)*(v*m_in_pc)**2) # Joules
  start = time() 
  for step in range(MAX_STEPS): 
    print "Step (%i/%i)\r" % (step,MAX_STEPS),
    stdout.flush() 
    
    if treeAlgorithm:
      # initialize tree
      tree = Tree(x,m,xcb,L) 
      # execute acceleration for each particle
      for i in range(N): 
        a[i] = accelerate(x[i],tree,epsilon)
      del tree
    else: 
      # execute acceleration for each particle
      for i in range(N): 
        a[i] = nsq_accelerate(x[i],x,m,epsilon)
    
    # print a; raise SystemExit
    
    # update velocity and position, TODO: eventually with RK4
    if 1: # Eulerian advancement
      v = v + dt*a
      x = x + dt*v
    else: # RK4 or leapfrog ... 
      print "ERROR: advancement method not set!"
      raise SystemExit
    
    # correct x values that went off-screen: 
    # with cyclic bcs, use sawtooth: 
    x = x-L*np.floor(.5+x/L)
    
    if step % output_frequency==0 and MAX_STEPS>1: 
      # save results occasionally 
      # if step mod 10 or so is zero
      # save position and velocity data
      if 0:
        savetxt(output_fname % step,
              np.concatenate((x,v),axis=1),delimiter=',',
              header="position data <x,y,z>, velocity data <vx,vy,vz>;"\
                   + " N=%i, L=%g" % (N,L))
      else: # save as pandas df, along with mass!
        # df = pd.DataFrame({"x":x,"v":v,"m":m})
        df = pd.DataFrame({"x":x[:,0],"y":x[:,1],"z":x[:,2],
                           "vx":v[:,0],"vy":v[:,1],"vz":v[:,2],
                           "m":m[:,0]})
        df.to_csv(output_fname % step,index=False)
  
  end = time()
  KE_final = .5*np.sum((m*M_solar)*(v*m_in_pc)**2) # Joules
  # print (KE_initial,KE_final)
  # fractional_change = (KE_final-KE_initial)/KE_initial
  # print 'frac change:',fractional_change
  print "%s N=%i elapsed time: %g seconds, KE change: %g Joules" \
         % (csvtag,N,(end-start),(KE_final-KE_initial))



