#!/bin/python
# calculate and plot energy for simulation

# imports 
from sys import argv

import numpy as np
import pandas as pd
import barnes_hut as bh

# read in file type (bh vs n2)
if len(argv)<2 or argv[1]=="bh" or argv[1]=="1":
  treeAlgorithm = 1
  csvtag = "bh"
  print "Running Barnes-Hut"
elif argv[1]=="n2" or argv[1]=="0":
  treeAlgorithm = 0
  csvtag = "n2"
  print "Running N^2"
else: 
  print "ERROR: invalid mode"
  raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
fnames = bh.output_dir + 'out_%s_%%05i.csv' % csvtag

def data_from_fname(fname,numpy=False):
  if numpy: # old scheme
    data = np.loadtxt(fname,delimiter=',')
    x,y,z,vx,vy,vz = np.transpose(data)
    xvec = np.transpose([x,y,z])
    vvec = np.transpose([vx,vy,vz])
    mvec = np.ones(len(x))
  else:
    print fname
    df = pd.read_csv(fname)
    xvec = np.transpose([df.x,df.y,df.z])
    vvec = np.transpose([df.vx,df.vy,df.vz])
    mvec = df.m
  return xvec,vvec,mvec

# calculate KE
def calc_KE(xvec,vvec,mvec):
  return .5 * mvec * bh.M_solar * bh.m_in_pc**2 * \
          (vvec[:,0]**2 + vvec[:,1]**2 + vvec[:,2]**2)

def calc_KE_tot(xvec,vvec,mvec):
  return np.sum(calc_KE(xvec,vvec,mvec))

def calc_PE_tot(xvec,vvec,mvec):
  # vectorized GPE calculation
  # code up later with BH?
  report = 0
  N = len(xvec)
  for ii in range(N-1):
    x_i = xvec[ii]
    dx = np.sqrt((x_i[0]-xvec[ii+1:,0])**2 \
               + (x_i[1]-xvec[ii+1:,1])**2 \
               + (x_i[2]-xvec[ii+1:,2])**2)
    report += mvec[ii]*np.sum(mvec[ii+1:]/dx)
  return -bh.G*report*bh.M_solar*10**6


if __name__=='__main__': 
  # use first data file for alpha testing
  # fname = 'runs/out_bh_00000.csv'
  N_tot = 5000
  di = 5
  count_ii = N_tot//di
  ke_ii = np.zeros(count_ii)
  pe_ii = np.ones(count_ii)
  for ii,i in enumerate(range(0,N_tot+1,di)):
    try:
      xvec,vvec,mvec = data_from_fname(fnames % i)
    except IOError: # file DNE; truncate output
      print "File DNE; truncating..."
      count_ii = ii
      ke_ii = ke_ii[:ii]
      pe_ii = pe_ii[:ii]
      break
    if 0: # print step by step
      KE = calc_KE_tot(xvec,vvec,mvec)
      GPE = calc_PE_tot(xvec,vvec,mvec)
      print "KE_tot:",KE
      print "GPE_tot:",GPE
    ke_ii[ii]=calc_KE_tot(xvec,vvec,mvec) 
    pe_ii[ii]=calc_PE_tot(xvec,vvec,mvec) 
    if ii+1>=count_ii: break
  print "plotting"
  from matplotlib import pyplot as plt
  plt.plot(np.arange(count_ii)*di,ke_ii,label='KE')
  plt.plot(np.arange(count_ii)*di,pe_ii,label='PE')
  etot = ke_ii + pe_ii
  plt.plot(np.arange(count_ii)*di,etot,label='PE+KE')
  plt.legend()
  plt.xlabel(r'Time Step')
  plt.ylabel(r'Energy (J)')
  plt.tight_layout()
  plt.show()
  
  plt.plot(np.arange(count_ii)*di,-ke_ii/pe_ii,label='-KE/PE')
  if 0: plt.plot(np.arange(count_ii)*di,-2*ke_ii/pe_ii,label='-2KE/PE')
  plt.plot(np.arange(count_ii)*di,etot/etot[0],label='energy change')
  plt.plot([0,count_ii*di],[1,1],'k')
  plt.plot([0,count_ii*di],[.5,.5],'grey')
  plt.legend()
  plt.xlabel(r'Time Step')
  plt.ylabel(r'Relative Energy')
  plt.tight_layout()
  plt.show()
