#!/usr/bin/env python
########################################################################
# n-body code 0.0
# author: William Kevin Black
# modified: 23 Feb 2019
########################################################################
from numpy import pi
from numpy import sqrt
# from scipy.integrate import quad as integrate
# from numpy import inf
# from sys import argv
########################################################################

########################################################################
# parameters 
########################################################################

# universal parameters 
c = 299792458                           # m/s exactly 
pc = 648000/pi                          # au 
au = 149597870700                       # m
m_in_km = 1000                          # exact
m_in_Mpc = pc*au*10**6                  # exact
km_in_pc = m_in_Mpc*10**-9              # exact
pc_in_Mpc = 1e6                         # exact
s_in_Myr = 60*60*24*365.2422*10**6      # apx
G_SI = 6.67408e-11                      # m^3/kg/s^2 # apx
G_SI_err = 3.1e-15                      # m^3/kg/s^2
M_solar = 1.98847e30                    # kg # apx
M_solar_err = 7e25                      # kg

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# derived parameters

G_astro = G_SI/m_in_Mpc*M_solar/m_in_km**2*pc_in_Mpc # pc.(km/s)^2/M_sol
G_astro_err = G_astro*sqrt((G_SI_err/G_SI)**2+(M_solar_err/M_solar)**2)
if 1: 
  print "G_astro:",(G_astro*1e3),"\be-3 pc M_sol^-1 (km/s)^2"
  print "G_astro_err:",(G_astro_err*1e3),"\be-3 pc M_sol^-1 (km/s)^2"
  raise SystemExit

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# simulation parameters 

output_frequency = 10              # how many steps between saving data
N = 25                             # number of particles in simulation

# 5e-6 pc :: 1 AU
#   1  pc :: Solar System
#  32 kpc :: Milky Way
# 3.1 Mpc :: Local Group
# 159 Mpc :: Laniakea
L = 5e-6                           # size of simulation in pc 
epsilon = L*1e-9                   # lowest probing distance 
# (prevents divide by zero in acceleration calculation)

V = 500/km_in_pc*1e-50             # initial velocity 1-sigma in pc/s
# h = 60*60                          # time step (hr) in seconds 
# h = 60*60*24                       # time step (day) in seconds 
h = 60*60*24*14                    # time step (14 days) in seconds 
# h = 60*60*24*365.2422              # time step (yr) in seconds 
# h = h*365.2422*10**6               # time step (Myr) in seconds 

MAX_STEPS = 1000                   # maximum time steps taken 
plotting = 0                       # flag to plot continuously 

# end parameters 

########################################################################
# calculation 

from numpy.random import rand
from numpy.random import normal
from numpy import ones 
from numpy import zeros
from numpy import concatenate
from numpy import savetxt

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# set ICs

pos = L*rand(N,3)-L/2
vel = normal(0,V,(N,3))
acl = zeros((N,3)) # this is a placeholder: this value is calculated
m = ones(N) # initialize all to single solar mass

def distsq(v1,v2):
  # returns the distance squared between two vectors
  # val =   (v2[0]-v1[0])**2 \
  #        + (v2[1]-v1[1])**2 \
  #        + (v2[2]-v1[2])**2 
  # print val,epsilon
  return   (v2[0]-v1[0])**2 \
         + (v2[1]-v1[1])**2 \
         + (v2[2]-v1[2])**2 

########################################################################
# begin simulation 

from sys import stdout # to make pretty print statements
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from numpy import linspace

if plotting: # plot IC
  # set up plot
  plt.ion()
  fig = plt.figure()
  ax1 = fig.add_subplot(121,projection='3d') 
  ax2 = fig.add_subplot(122,projection='3d') 
  # ax = Axes3D(fig)
  colors = cm.rainbow(linspace(0, 1, N))
  ax1.set_xlim(-L/2,L/2)
  ax1.set_ylim(-L/2,L/2)
  ax1.set_zlim(-L/2,L/2)
  ax2.set_xlim(-L/2,L/2)
  ax2.set_ylim(-L/2,L/2)
  ax2.set_zlim(-L/2,L/2)
  
  def update_plot(q,col): 
    ax1.scatter(q[:,0], q[:,1], q[:,2], c=col)
    ax2.scatter(q[:,0], q[:,1], q[:,2], c=col)
    viewangle = 45
    btweyes = 5
    ax1.view_init(30, viewangle-btweyes)
    ax2.view_init(30, viewangle+btweyes)
    plt.draw()
    plt.pause(0.001)
  
  # display initial conditions 
  update_plot(pos,colors)

for step in range(MAX_STEPS): 
  print "Step (%i/%i)\r" % (step,MAX_STEPS),
  stdout.flush()
  # calculate force on each object
  # perhaps plot new positions 
  # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
  # update acceleration
  # a = F/m = G*sum(m[i]*(q2-q1)/distsq(q2,q1)**(3/2) 
  for i in range(N): 
    q1 = pos[i]
    a_sum = 0
    for j in range(N): 
      if j==i: 
        # no acceleration from itself
        continue
      
      # good ol' RK4: 
      # http://spiff.rit.edu/richmond/nbody/OrbitRungeKutta4.pdf
      
      q2 = pos[j]
      a_sum += m[j]*(q2-q1)/(distsq(q2,q1)+epsilon)**(3/2)
    acl[i] = G_astro*a_sum/km_in_pc**2 # in pc/s^2
  
  print acl; exit() 
  
  # update velocity and position, TODO: eventually with RK4
  vel = vel + h*acl # pc/s
  pos = pos + h*vel # pc/s

  if plotting: # update display 
    update_plot(pos,colors)
  
  if step % output_frequency==0: 
    # save position (and velocity?) data
    savetxt('out_n2_%05i.csv' % step,
            concatenate((pos,vel),axis=1),delimiter=',',
            header="position data <x,y,z>, velocity data <vx,vy,vz>;"\
                 + " N=%i, L=%g" % (N,L))
