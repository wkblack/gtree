#!/bin/python 
# plot output from n_body.py 
live=0 # live plotting---watch as it goes 

from glob import glob
files=sorted(glob('out_*.csv'))

from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from numpy import linspace
from numpy import loadtxt 

# set up plot
plt.ion()
fig = plt.figure()
ax1 = fig.add_subplot(121,projection='3d') 
ax2 = fig.add_subplot(122,projection='3d') 
# ax = Axes3D(fig)

# read off N and L
from re import findall
numstr = "[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?"
with open(files[0]) as f: 
  [N,L]=findall(numstr,f.next())
  N=int(N)
  L=float(L) 

colors = cm.rainbow(linspace(0, 1, N))
ax1.set_xlim(-L/2,L/2)
ax1.set_ylim(-L/2,L/2)
ax1.set_zlim(-L/2,L/2)
ax2.set_xlim(-L/2,L/2)
ax2.set_ylim(-L/2,L/2)
ax2.set_zlim(-L/2,L/2)
  
def update_plot(q,col): 
  ax1.scatter(q[:,0], q[:,1], q[:,2], c=col)
  ax2.scatter(q[:,0], q[:,1], q[:,2], c=col)
  viewangle = 45
  btweyes = 5
  ax1.view_init(30, viewangle-btweyes)
  ax2.view_init(30, viewangle+btweyes)
  if live: 
    plt.draw()
    plt.pause(0.001)

for f in files:
  pos=loadtxt(f,skiprows=1,delimiter=',') 
  update_plot(pos,colors)

plt.savefig('out_sum.png')
