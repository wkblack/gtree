#!/usr/bin/env python
########################################################################
# n-body code 0.0
# author: William Kevin Black
# modified: 23 Feb 2018
########################################################################
from numpy import pi
# from numpy import sqrt
# from scipy.integrate import quad as integrate
# from numpy import inf
# from sys import argv
########################################################################

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
# parameters 

# universal parameters 
c = 299792458 # m/s exactly 
pc = 648000/pi # au 
au = 149597870700 # m
m_in_Mpc = pc*au*10**6 # exact
s_in_Myr = 60*60*24*365.25*10**6 # apx
G_SI = 6.67408e-11 # m^3/kg/s^2 # apx

# derived parameters
G_astro = G_SI/m_in_Mpc

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # 
