MPI routines for julia :) 

https://github.com/JuliaParallel/MPI.jl/blob/master/examples/01-hello-impl.jl
function do_hello()
    comm = MPI.COMM_WORLD
    println("Hello world, I am $(MPI.Comm_rank(comm)) of $(MPI.Comm_size(comm))")
    MPI.Barrier(comm)
end



Overview of julia / getting started guide to v1
https://docs.julialang.org/en/v1/manual/control-flow/

